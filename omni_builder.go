package btctx

import (
	"errors"
	"fmt"

	"gitee.com/coin-kit/btc-tx/address"
	"github.com/btcsuite/btcd/wire"
)

type OmniBuilder struct {
	builder *Builder
}

func NewOmniBuilder() *OmniBuilder {
	obuilder := new(OmniBuilder)
	obuilder.builder = NewBuilder()
	return obuilder
}

func (ob *OmniBuilder) SetFee(fee uint64) {
	if ob.builder.err != nil {
		return
	}
	ob.builder.SetFee(fee)
	return
}

func (ob *OmniBuilder) SetParams(params *address.Params) {
	if ob.builder.err != nil {
		return
	}
	ob.builder.SetParams(params)
	return
}

func (ob *OmniBuilder) SetRefund(wifaddr string) {
	if ob.builder.err != nil {
		return
	}
	ob.builder.SetRefund(wifaddr)
	return
}

func (ob *OmniBuilder) SetCoins(coins []*Coin) {
	if ob.builder.err != nil {
		return
	}
	ob.builder.SetCoins(coins)
	return
}

func (ob *OmniBuilder) AddTxOmniOut(wifaddr string, token uint32, amount uint64, gift uint64) {
	if ob.builder.err != nil {
		return
	}
	if ob.builder.params == nil {
		ob.builder.err = errors.New("No set of address params")
		return
	}
	if ob.builder.refund == "" {
		ob.builder.err = errors.New("refund address is empty")
		return
	}
	ob.builder.outputs = make([]*wire.TxOut, 0, 3)
	// 建立来源地址输入
	addr, err := address.FromWIFAddress(ob.builder.refund, ob.builder.params)
	if err != nil {
		ob.builder.err = err
		return
	}
	pk_script := addr.ToPkScript()
	ob.builder.outputs = append(ob.builder.outputs, wire.NewTxOut(int64(0), pk_script))
	// 建立币值输出
	omni := newOmni(token, amount)
	script := omni.ToScript()
	ob.builder.outputs = append(ob.builder.outputs, wire.NewTxOut(0, script))
	// 建立接受地址输出
	addr, err = address.FromWIFAddress(wifaddr, ob.builder.params)
	if err != nil {
		ob.builder.err = err
		return
	}
	pk_script = addr.ToPkScript()
	ob.builder.outputs = append(ob.builder.outputs, wire.NewTxOut(int64(gift), pk_script))
	return
}

func (ob *OmniBuilder) Build() ([]byte, error) {
	if ob.builder.err != nil {
		return nil, ob.builder.err
	}
	// 计算总输出
	if len(ob.builder.outputs) != 3 {
		return nil, errors.New("omni token transaction txout only is 3")
	}
	out_total := ob.builder.fee
	for _, item := range ob.builder.outputs {
		out_total = out_total + uint64(item.Value)
	}
	// 挑选最佳输入
	in_total := uint64(0)
	used := []*Coin{}
	for i := 0; i < len(ob.builder.coins) && in_total < out_total; i++ {
		coin := ob.builder.coins[i]
		used = append(used, coin)
		in_total = in_total + coin.Amount
	}
	ob.builder.coins = used
	// 配平
	if in_total < out_total {
		return nil, fmt.Errorf("shortage of coin %d < %d %d", in_total, out_total, len(ob.builder.coins))
	}
	// 找零
	balance := in_total - out_total
	ob.builder.outputs[0].Value = int64(balance)
	// 构建
	tx := wire.NewMsgTx(1)
	tx.LockTime = 0
	tx.TxOut = ob.builder.outputs
	tx.TxIn = []*wire.TxIn{}
	for _, coin := range ob.builder.coins {
		txin, err := coin.ToTxIn()
		if err != nil {
			return nil, err
		}
		tx.TxIn = append(tx.TxIn, txin)
	}
	return EncodeTxBytes(tx)
}
