package btctx

import (
	"encoding/hex"

	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/wire"
)

type Coin struct {
	TxHash   string
	Index    uint32
	PkScript string
	Amount   uint64
}

func (c *Coin) ToTxIn() (*wire.TxIn, error) {
	txhash, err := chainhash.NewHashFromStr(c.TxHash)
	if err != nil {
		return nil, err
	}
	pk_script, err := hex.DecodeString(c.PkScript)
	if err != nil {
		return nil, err
	}
	op := wire.NewOutPoint(txhash, c.Index)
	// 这里需要把pk_script暂存于这里，签名之后以sig_script替换
	txin := wire.NewTxIn(op, pk_script, nil)
	return txin, nil
}
