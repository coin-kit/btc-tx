package address

//Params is parameters of the coin.
type Params struct {
	AddressHeader byte
	P2SHHeader    byte
}
