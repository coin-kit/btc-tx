package address

var (
	//BitcoinMain is params for main net.
	BitcoinMain = &Params{
		AddressHeader: 0,
		P2SHHeader:    5,
	}
	//BitcoinTest is params for test net.
	BitcoinTest = &Params{
		AddressHeader: 111,
		P2SHHeader:    196,
	}
	//LitecoinMain is params for main net.
	LitecoinMain = &Params{
		AddressHeader: 48,
		P2SHHeader:    50,
	}
)
