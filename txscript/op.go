package txscript

import (
	"encoding/hex"
)

type OP struct {
	code byte
	data []byte
}

func NewOP(code byte, data []byte) OP {
	return OP{
		code: code,
		data: data,
	}
}

func (o OP) GetData() []byte {
	return o.data
}

func (o OP) String() string {
	buf := ""
	buf = buf + Names[o.code]
	if len(o.data) > 0 {
		buf = buf + "  " + hex.EncodeToString(o.data)
	}
	return buf
}
