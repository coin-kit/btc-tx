package txscript

func DefaultP2PKHScript(pkh []byte) *Script {
	script := NewEmptyScript()
	script.PushOP(NewOP(OP_DUP, nil))
	script.PushOP(NewOP(OP_HASH160, nil))
	script.PushOP(NewOP(OP_DATA_20, pkh))
	script.PushOP(NewOP(OP_EQUALVERIFY, nil))
	script.PushOP(NewOP(OP_CHECKSIG, nil))
	return script
}

func DefaultP2SHScript(sh []byte) *Script {
	script := NewEmptyScript()
	script.PushOP(NewOP(OP_HASH160, nil))
	script.PushOP(NewOP(OP_DATA_20, sh))
	script.PushOP(NewOP(OP_EQUAL, nil))
	return script
}
