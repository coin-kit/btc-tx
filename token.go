package btctx

import (
	"bytes"
	"encoding/binary"

	"gitee.com/coin-kit/btc-tx/txscript"
)

type Omni struct {
	magic   uint32
	version uint16
	txtype  uint16
	token   uint32
	amount  uint64
}

func newOmni(token uint32, amount uint64) *Omni {
	o := new(Omni)
	o.magic = 0x6f6d6e69
	o.version = 0x00
	o.txtype = 0x00
	o.token = token
	o.amount = amount
	return o
}

func (o *Omni) ToScript() []byte {
	// 编码数据
	buffer := bytes.NewBuffer(nil)
	binary.Write(buffer, binary.BigEndian, o.magic)
	binary.Write(buffer, binary.BigEndian, o.version)
	binary.Write(buffer, binary.BigEndian, o.txtype)
	binary.Write(buffer, binary.BigEndian, o.token)
	binary.Write(buffer, binary.BigEndian, o.amount)
	// 构建脚本
	script := txscript.NewEmptyScript()
	script.PushOP(txscript.NewOP(txscript.OP_RETURN, nil))
	script.PushOP(txscript.NewOP(txscript.OP_DATA_20, buffer.Bytes()))
	// 返回脚本
	return script.Bytes()
}
